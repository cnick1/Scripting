figure
fftVisualTime=time;
fftVisualSignal=data(:,1);

fftSignalLength = length(fftVisualTime);      % Signal length
fftFrequencyStep=1/(fftVisualTime(2)-fftVisualTime(1));

fftVisualN = 2^(nextpow2(fftSignalLength)+4);

fftVisualY = fft(fftVisualSignal,fftVisualN);

fftVisualF = fftFrequencyStep*(0:(fftVisualN/2))/fftVisualN;
fftVisualP = abs(fftVisualY/fftSignalLength*2);

plot(fftVisualF,fftVisualP(1:fftVisualN/2+1)) 
title('Signal in Frequency Domain')
xlabel('Frequency in Hz')
ylabel('|P(f)|')

[maximum, index] = max(fftVisualP(1:fftVisualN/2+1));
center_freq = fftVisualF(index);
fprintf('The amplitude is %2.2f at center frequency %2.2f/n', maximum, center_freq)
