function[t,y]=Toneburst(center_freq,ncycles)
dt=1/50/center_freq;
win_freq=center_freq/ncycles/2;
tf=2^7*dt;
t=0:dt:(tf-dt); 
offset=1;
t0=offset*dt;
npoints=(ncycles/center_freq)/dt;
t1=t(offset+1:npoints+offset+1);
y=0*t;
y1=sin(2*pi*center_freq*(t1-t0));
y2=cos(2*pi*win_freq*(t1-t0)+3*pi/2);
y(offset:npoints+offset)=y1.*y2;
y=y'; t=t';