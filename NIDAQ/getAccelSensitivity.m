clear all
close all
clc

% devices = daq.getDevices              %Used to determine connected DAQ

s = daq.createSession('ni');          %Initializes data collection
daq_ch0 = addAnalogInputChannel(s,'cDAQ9184Mod2', "ai0", 'IEPE'); %Sets channels on NIDAQ
daq_ch0.Coupling = 'AC';
% ========Parameters=================
s.Rate = 25600; %25601             %Sampling Rate (Hz)
s.DurationInSeconds = 1;          %File record time
% ====================================

disp('Starting Acquisition...');
[data,time] = s.startForeground;   %Data collection - use listener to see live feed

sens_accel = max(abs(data(:,1))); %V/g
rms_sensitivity = sqrt(sum(data.*data)/length(data))*sqrt(2);
plot(time,data(:,1)/sens_accel);
hold on
plot(time,data(:,1)/rms_sensitivity);
ylabel('Acceleration (g)');
xlabel('Time (s)')
xlim([0 2/159])

figure
fftVisualTime=time;
fftVisualSignal=data(:,1)/rms_sensitivity;

fftSignalLength = length(fftVisualTime);      % Signal length
fftFrequencyStep=1/(fftVisualTime(2)-fftVisualTime(1));

fftVisualN = 2^(nextpow2(fftSignalLength)+4);

fftVisualY = fft(fftVisualSignal,fftVisualN);

fftVisualF = fftFrequencyStep*(0:(fftVisualN/2))/fftVisualN;
fftVisualP = abs(fftVisualY/fftSignalLength*2);

plot(fftVisualF,fftVisualP(1:fftVisualN/2+1)) 
title('Signal in Frequency Domain')
xlabel('Frequency in Hz')
ylabel('|P(f)|')
xlim([0 320])

[maximum, index] = max(fftVisualP(1:fftVisualN/2+1));
center_freq = fftVisualF(index);
fprintf('\n \n With a sensitivity of %4.2f mV/g, the amplitude is %2.2f g at center frequency %2.2f \n \n \n', rms_sensitivity*1000, maximum, center_freq)