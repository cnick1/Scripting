clear all
close all
clc

devices = daq.getDevices              %Used to determine connected DAQ

s = daq.createSession('ni');          %Initializes data collection
daq2_ch0 = addAnalogInputChannel(s,'cDAQ1Mod1', 1, 'IEPE'); %Sets channels on NIDAQ

% ========Parameters=================
daq2_ch0.Coupling = 'AC';
sens_accel = 1; %V/g

s.Rate = 25600; %25601             %Sampling Rate (Hz)
s.DurationInSeconds = 1;          %File record time
% ====================================

disp('Starting Acquisition...');
[data,time] = s.startForeground;   %Data collection - use listener to see live feed

plot(time,data(:,1)/sens_accel);
ylabel('Acceleration (g)');
xlabel('Time (s)')
xlim([0 .01])